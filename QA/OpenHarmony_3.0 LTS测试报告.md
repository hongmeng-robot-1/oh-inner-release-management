

修订记录

 

| 日期 | 修订版本 | 修改章节 | 修改描述 |
| ---- | -------- | -------- | -------- |
|   2021-9-28   |  V1.0        |          |   刷新问题闭环状态       |

缩略语清单： 

| 缩略语 | 英文全名 | 中文解释 |
| ------ | -------- | -------- |
|        |          |          |
|        |          |          |
|        |          |          |
|        |          |          |

# 

# 1   概述

本报告描述针对OpenHarmony 2.2 beta2 版本以来新增能力的验证及质量状态（新增能力参考章节4.1.2描述）。包括对历史能力的基本能力覆盖。

# 2   测试版本说明

描述测试版本信息。

| 版本名称 | 测试起始时间 | 测试结束时间 |
| -------- | ------------ | ------------ |
|      OpenHarmony 3.0 LTS    |       2021-9-18       |        2021-9-28      |

描述本次测试的测试环境（包括环境软硬件版本信息，环境组网配置信息, 测试辅助工具等）。

| 硬件型号 | 硬件配置信息 | 备注 |
| -------- | ------------ | ---- |
|     HI3516DV300    |              |  针对电话服务能力需要单独编译外挂modem的版本验证    |
|       RK3568   |              |  仅支持对GPU合成渲染能力的验证   |
|      T618    |              |  仅验证蓝牙能力   |

 

# 3   版本概要测试结论

自拟定报告为止，当前LTS版本基本能力可用，可以支撑开发者使用及演示。存在部分问题及约束影响商用，如下描述：
<br />1）HDC仍存在长时间开关机后，设备不可发现问题，无法支持稳定的自动化测试；
<br />2）分布式数据、文件相关能力当前手工测试正常，测试用例自动化批量执行时环境异常，当前正在定位；
<br />3）LTS版本基础性能对比Beta版本有劣化，含：标准系统在设置、图库、商城、闹钟、计算机、相机、音乐的启动时延大，该问题当前不影响三方应用开发及演示，如果后续三方应用商用则需要在产品化过程中持续优化；
<br />4）蓝牙能力、电话能力不可直接在当前版本上使用，需要定制软件和定制硬件版本才可正常使用，详细情况参考readme说明；
<br />5）软总线能力存在特定场景下的可靠性建链问题，如反复链接30次后设备不可发现；
<br />6）当前公板不支持更新内核，因此轻量级内核无法在社区公板上进行使用，需要开发者定制可更新内核的硬件适配使用；
<br />7）libhitrace,libhicollie仅支持基本能力，不支持日志信息落盘存储，在后续版本中增强；

# 4   版本详细测试结论

*本章节针对总体测试策略计划的测试内容，给出详细的测试结论。*

## 4.1   特性测试结论



### 4.1.1   继承特性评价

*继承特性进行评价，用表格形式评价，包括特性列表，验证质量评估。*

XXX子系统：特性质量良好

| 序号 | 特性名称    | 特性质量评估                             | 备注 |
| ---- | ----------- | ---------------------------------------- | ---- |
| 1  | 启动恢复子系统 | 没有新增特性，测试用例执行通过，特性质量良好 |      |
| 2 | 内核子系统 | 轻量系统无合适的公共开发板，该系统内核特性无法验证，风险高<br />小型系统稳定性压力测试存在文件删除，内存泄漏等问题，trace功能缺少指导书，toybox支持的shell命令缺少使用手册，开发者使用难度较大，风险高；<br />标准系统基本功能可用，特性质量良好<br /> | 约束：支持toybox shell命令的rootfs需要刷mksh_rootfs_vfat.img，区别于rootfs_vfat.img<br />问题：<br />[【OpenHarmony】【3.0.0.8】【轻内核子系统】集成测试I3NT3F内核支持trace功能缺少指导书无法测试，开发者使用难度较大 ](https://gitee.com/openharmony/kernel_liteos_a/issues/I4BN00)<br/>[【OpenHarmony】【3.0.0.8】【轻内核子系统】dyload_posix模块在removefile的时候出现错误 ](https://gitee.com/openharmony/kernel_liteos_a/issues/I4BJFU)<br/>[【OpenHarmony】【3.0.0.2】【轻内核子系统】集成测试toybox需提供用户手册，手册中需要将各命令的限制详细说明，以便更好的指导... ](https://gitee.com/openharmony/third_party_toybox/issues/I44YLY)<br/>[【OpenHarmony】【20210701】【轻内核子系统】xts权限用例压测用户态概率异常 ](https://gitee.com/openharmony/kernel_liteos_a/issues/I3ZJ1D)<br/>[【OpenHarmony】【20210726】【轻内核子系统】集成测试直接执行cat后无法退出，需要重启设备恢复](https://gitee.com/openharmony/third_party_mksh/issues/I42N33) |
| 3 | 媒体子系统 | 标准系统历史特性（音视频播放、音频管理、相机拍照）验证通过，特性质量良好<br />轻量小型系统无新增特性，测试用例执行通过，特性质量良好 |      |
| 4 | 驱动子系统 | 小型系统没有新增特性，测试用例执行通过，特性质量良好；<br />标准系统基本功能可用，camera驱动遗留少量问题; | |
| 5 | 泛sensor服务子系统 | 小型系统没有新增特性，测试用例执行通过，特性质量良好； | |
|6|用户程序框架子系统|小型系统没有新增特性，测试用例执行通过，特性质量良好||
|7|账号子系统|特性基本可用||
|8|杂散子系统|特性基本可用||
|9|元能力子系统|特性基本可用||
|10|事件通知子系统|标准系统没有新增特性，测试用例执行通过，特性质量良好||
|11|窗口子系统|特性基本可用||
|12|电话子系统|特性基本可用||
|12|分布式文件子系统|特性基本可用，基本功能验证通过，自动化测试批量加载执行有问题，当前正在定位，不影响特性正常使用，xts用例暂时无法正常发布||
|13|DFX子系统|基本功能可用|native测试用例压测执行不通过，发现遗留问题：<br>1、运行hilog的压测，hilogd异常重启，且hilog命令一直无法使用<br>2、js测试用例压测执行通过，特性质量良好<br>https://gitee.com/openharmony/hiviewdfx_hilog/issues/I48IM7?from=project-issue|
|14| 电源子系统 | 无新增特性，测试用例执行通过，特性质量良好 | |
|15|安全子系统|特性基本可用||
|16|分布式数据库子系统|特性基本可用，基本功能验证通过，自动化测试批量加载执行有问题，当前正在定位，不影响特性正常使用，xts用例暂时无法正常发布||
|17|软总线子系统|继承特性：基于wifi的coap发现、自组网和Message/Byte传输<br/>特性质量：<br/>(1) 功能可用，具备一定的可靠性和稳定性，软总线遗留问题（涉及特定场景下的功能和性能）已评估，风险中 | 1、软总线遗留问题(已评估)：[I4492M](https://gitee.com/openharmony/communication_dsoftbus/issues/I4492M),[I477AF](https://gitee.com/openharmony/communication_dsoftbus/issues/I477AF),[I49HQ3](https://gitee.com/openharmony/communication_dsoftbus/issues/I49HQ3),[I49HN6](https://gitee.com/openharmony/communication_dsoftbus/issues/I49HN6) <br/>2、其他领域问题影响软总线稳定性 <br/> 短距：[I4BVUL](https://gitee.com/openharmony/communication_dsoftbus/issues/I4BVUL) <br/>HiChain：[I48YPH](https://gitee.com/openharmony/communication_dsoftbus/issues/I48YPH)，[I4BW6E](https://gitee.com/openharmony/communication_dsoftbus/issues/I4BW6E)，[I4BVVW](https://gitee.com/openharmony/communication_dsoftbus/issues/I4BVVW)<br/> |
|18|图形子系统|支持vsync,bufferqueue,支持CPU合成特性。提供基础的图形显示能力。在测试demo与系统应用均测试过，界面能正常显示，demo正常运行。特性质量良好||
|19|ACE子系统|JS与NAPI混合开发：特性测试通过，功能正常；<br/>video组件适配：组件可以使用，但进度条不显示；<br/>camera组件支持：组件可以使用，但录像功能异常，音画不同步，有杂音，声音断续卡顿问题。|video组件适配ISSUE单https://gitee.com/openharmony/graphic_standard/issues/I44W7U?from=project-issue；<br />camera组件支持ISSUE单https://gitee.com/openharmony/multimedia_camera_standard/issues/I42TMC?from=project-issue；<br />camera组件支持ISSUE单：https://gitee.com/openharmony/multimedia_media_standard/issues/I42TIB?from=project-issue|

*特性质量评估可选项：特性不稳定，风险高\特性基本可用，遗留少量问题\特性质量良好*

### 4.1.2   新需求评价

*以表格的形式汇总新特性测试执行情况及遗留问题情况的评估,给出特性质量评估结论。*

| lssue号 | 特性名称 | 特性质量评估 | 约束依赖说明 | 备注 |
| ------- | -------- | ------------ | ------------ | ---- |
| [I3ZVTJ](https://gitee.com/open_harmony/dashboard?issue_id=I3ZVTJ)  |【分布式任务调度子系统】接收远端拉起FA请求，跨设备拉起远端FA|特性基本可用|提供标准系统拉起标准系统远端FA的功能，缺少使用指导书，开发者使用困难，风险高   |     |
| [I3ZVTT](https://gitee.com/open_harmony/dashboard?issue_id=I3ZVTT)  |【分布式任务调度子系统】标准系统分布式能力建设-SAMGR模块构建 |特性基本可用|   |  |
| [I3ZVT4](https://gitee.com/open_harmony/dashboard?issue_id=I3ZVT4)  |【分布式任务调度子系统】标准系统分布式能力建设-跨设备绑定元能力|特性基本可用| 提供标准系统绑定标准系统远端元能力的功能。当前缺少使用指导说明，开发者使用较为困难，风险高 |  |
| [I49HB0](https://gitee.com/open_harmony/dashboard?issue_id=I49HB0)  |【分布式任务调度子系统】启动、绑定能力添加组件visible权限校验| 特性基本可用 |   |  提供标准系统与标准系统之间组件校验能力    |
| [I3NT3F](https://gitee.com/openharmony/kernel_liteos_a/issues/I3NT3F) | 【轻内核子系统】内核支持trace功能 | 缺少使用指导书，开发者使用困难，风险高 |              |      |
| [I3NT63](https://gitee.com/openharmony/kernel_liteos_a/issues/I3NT63) | 【轻内核子系统】pagecache功能完善 | 特性基本可用，遗留少量问题 |              |      |
| [I3ZY55](https://gitee.com/open_harmony/dashboard?issue_id=I3ZY55) | 【Kernel升级适配】Hi3516DV300开发板适配OpenHarmony 5.10内核 | 特性质量良好 | | |
| I40MWK | 构建语言和地区配置和区域显示能力 | 特性基本可用 | | |
| I46W6Q | 【全球化】新增31种语言支持 | 特性基本可用 | | |
| [I3NT3F](https://gitee.com/openharmony/multimedia_media_standard/issues/I42TIB) |【媒体子系统】[Media][媒体录制器]音视频录制及API | 特性基本可用:音视频录制基本功能正常 |              | 遗留问题：<br />音频录制有杂音<br />(https://gitee.com/openharmony/multimedia_media_standard/issues/I4BXWY)|
| [I42TMC](https://gitee.com/openharmony/multimedia_camera_standard/issues/I42TMC) |【媒体子系统】[Camera][相机框架管理]相机录像功能 | 特性不稳定，风险高：基本录像功能可用但存在多个遗留问题 |              | 遗留问题：<br />1.视频录制后前几秒没声音，播放声画不同步，在板子播放会卡顿，音源较远时，杂音很大(https://gitee.com/openharmony/multimedia_camera_standard/issues/I4BXY1)<br />2.相机录制在图库播放时画面显示不全(https://gitee.com/openharmony/multimedia_camera_standard/issues/I4BXDQ)|
| I40OWT |【搜网】Radio状态管理 | 特性基本可用 | modem占用USB接口，导致USB HDC不可用。此为hi3516开发板约束。 | |
| I40OWU |【搜网】搜网注册 | 特性基本可用 | | |
| I40OU3 |【搜网】驻网信息(运营商信息及SPN广播GSM） | 特性基本可用 | | |
| I40OU5 |【搜网】网络状态(漫游、GSM/LTE/WCDMA接入技术） | 特性基本可用 | | |
| I40OU7 |【搜网】信号强度(WCDMA/GSM/LTE) | 特性基本可用 | | |
| I40OTY |【SIM卡】G/U卡文件信息获取、保存 | 特性基本可用 | | |
| I40OWN |【SIM卡】卡状态处理 | 特性基本可用 | | |
| I40OWO |【SIM卡】解锁卡pin、puk | 特性基本可用 | | |
| I40OWM |【SIM卡】卡账户处理 | 特性基本可用 | | |
| I40OSY |【SIM卡】提供拷贝、删除、更新、获取卡短信的能力供电话子系统内部使用 | 特性基本可用 | | |
| I40OUC |【短彩信】提供发送短信功能 | 特性基本可用 | | |
| I40OUF |【短彩信】提供接收短信功能 | 特性基本可用 | | |
| I40OX3 |【短彩信】提供小区广播的能力 | 特性基本可用 | | |
| [I48BOB](https://gitee.com/openharmony/distributeddatamgr_file/issues/I48BOB) |【分布式文件子系统】（需求）JS存储框架开发 | 特性基本可用 | | 基本功能验证通过，自动化测试批量加载执行有问题，当前正在定位，不影响特性正常使用，xts用例暂时无法正常发布 |
| [I4A3LY](https://gitee.com/openharmony/distributeddatamgr_file/issues/I4A3LY) |【分布式文件子系统】（需求）JS存储框架挂载能力 - FS Manager | 特性基本可用 | | 基本功能验证通过，自动化测试批量加载执行有问题，当前正在定位，不影响特性正常使用，xts用例暂时无法正常发布 |
|I4BXPQ |【DFX】libhicollie支持OpenHarmony standard |特性基本可用|libhicollie功能不完整，未实现日志落盘|      |
|I4BXMM |【DFX】libhitrace支持OpenHarmony standard|特性基本可用 |libhitrace功能不完整，上层Binder通信未适配，无法进行traceid传递|      |
|I4BY0R |【DFX】鸿蒙应用事件打点接口与鸿蒙应用事件管理（JS）|特性基本可用|功能不完整，无存储到DB|      |
| [I3ZVUD](https://gitee.com/open_harmony/dashboard?issue_id=I3ZVUD) | 【设备管理】提供设备认证、发现和查询的kit接口 | 基本功能可用 | 只支持运行在标准系统下的系统应用调用，其他不支持 | |
| [I3ZVIO](https://gitee.com/open_harmony/dashboard?issue_id=I3ZVIO) | 【设备管理】提供账号无关设备的PIN码认证能力| 基本功能可用：包括PIN码认证用户授权提示、PIN码显示、PIN码输入。PIN码输入弹框显示性能不佳，PIN码输入不流畅易误触 | 只支持运行在标准系统下的设备间认证组网，其他场景不支持| |
| [I40PAV](https://gitee.com/open_harmony/dashboard?issue_id=I40PAV) | 发布开启一个有页面的Ability的WantAgent通知 | 特性基本可用 | | |
| [I40PAW](https://gitee.com/open_harmony/dashboard?issue_id=I40PAW) | 通知删除接口 | 特性基本可用 | | |
| [I40PAX](https://gitee.com/open_harmony/dashboard?issue_id=I40PAX) | 查看Active通知内容和Active通知个数的接口 | 特性基本可用 | | |
| [I40PAY](https://gitee.com/open_harmony/dashboard?issue_id=I40PAY) | 通知取消订阅 | 特性基本可用 | | |
| [I40PAZ](https://gitee.com/open_harmony/dashboard?issue_id=I40PAZ) | 通知订阅 | 特性基本可用 | | |
| [I40PB6](https://gitee.com/open_harmony/dashboard?issue_id=I40PB6) | 应用侧增加slot | 特性基本可用 | | |
| [I40PB7](https://gitee.com/open_harmony/dashboard?issue_id=I40PB7) | 应用侧删除slot | 特性基本可用 | | |
| [I436VL](https://gitee.com/open_harmony/dashboard?issue_id=I436VL) | ces部件化改造 | 特性基本可用 | | |
| [I436VM](https://gitee.com/open_harmony/dashboard?issue_id=I436VM) | ans部件化改造 | 特性基本可用 | | |
| [I40PBC](https://gitee.com/open_harmony/dashboard?issue_id=I40PBC) | 应用侧发布本地分组的普通通知 | 特性基本可用 | | |
| [I40PBF](https://gitee.com/open_harmony/dashboard?issue_id=I40PBF) | 在免打扰模式下发布通知 | 特性基本可用 | | |
| [I40PBG](https://gitee.com/open_harmony/dashboard?issue_id=I40PBG) | 发布开启一个无页面的Ability的wantAgent | 特性基本可用 | | |
| [I40PBH](https://gitee.com/open_harmony/dashboard?issue_id=I40PBH) | 取消WantAgent的实例 | 特性基本可用 | | |
| [I40PBI](https://gitee.com/open_harmony/dashboard?issue_id=I40PBI) | 发布公共事件的WantAgent通知 | 特性基本可用 | | |
| [I40PBM](https://gitee.com/open_harmony/dashboard?issue_id=I40PBM) | 应用侧取消本地通知 | 特性基本可用 | | |
| [I40PBN](https://gitee.com/open_harmony/dashboard?issue_id=I40PBN) | 应用侧发布声音通知 | 特性基本可用 | | |
| [I40PBO](https://gitee.com/open_harmony/dashboard?issue_id=I40PBO) | 应用侧发布振动通知 | 特性基本可用 | | |
| [I40PBP](https://gitee.com/open_harmony/dashboard?issue_id=I40PBP) | 应用侧发布本地有输入框的通知（NotificationUserInput） | 特性基本可用 | | |
| [I40PBD](https://gitee.com/open_harmony/dashboard?issue_id=I40PBD) | 2个不同的slot，加入到同一个Slot组里面 | 特性基本可用 | | |
| [I40PBJ](https://gitee.com/open_harmony/dashboard?issue_id=I40PBJ) | 提供管理角标显示/隐藏的接口 | 特性基本可用 | | |
| [I40PBK](https://gitee.com/open_harmony/dashboard?issue_id=I40PBK) | 提供管理通知许可的接口（设置和查询） | 特性基本可用 | | |
| [I40PBL](https://gitee.com/open_harmony/dashboard?issue_id=I40PBL) | 应用删除SlotGroup | 特性基本可用 | | |
| [I40PBQ](https://gitee.com/open_harmony/dashboard?issue_id=I40PBQ) | 发布带ActionButton的本地通知 | 特性基本可用 | | |
| [I40PBR](https://gitee.com/open_harmony/dashboard?issue_id=I40PBR) | 通知流控处理 | 特性基本可用 | | |
| [I40PBT](https://gitee.com/open_harmony/dashboard?issue_id=I40PBT) | 死亡监听 | 特性基本可用 | | |
| [I40PBU](https://gitee.com/open_harmony/dashboard?issue_id=I40PBU) | 通知shell命令 | 特性基本可用 | | |
| [I40PBB](https://gitee.com/open_harmony/dashboard?issue_id=I40PBB) | 应用侧发布本地图片类型通知 | 特性不稳定 | [I4CDBR](https://gitee.com/openharmony/notification_ans_standard/issues/I4CDBR)【OpenHarmony】【LTS】【通知子系统】图片类型通知不可用 | |
| [I40PBS](https://gitee.com/open_harmony/dashboard?issue_id=I40PBS) | 通知图片大小限制 | 特性不稳定 | [I4CDBR](https://gitee.com/openharmony/notification_ans_standard/issues/I4CDBR)【OpenHarmony】【LTS】【通知子系统】图片类型通知不可用 | |
| [I4014F](https://gitee.com/open_harmony/dashboard?issue_id=I4014F) | 【帐号子系统】JS API交付，开源+小程序 | 特性基本可用 | | |
| [I43G6T](https://gitee.com/open_harmony/dashboard?issue_id=I43G6T) | WM Client架构演进 | 特性基本可用 | | |
| [I43HMM](https://gitee.com/open_harmony/dashboard?issue_id=I43HMM) | WM Server架构演进 | 特性基本可用 | | |
| [I40OPG](https://gitee.com/open_harmony/dashboard?issue_id=I40OPG) | 【定时服务】非功能性需求 | 特性基本可用 | | |
| [I40OPH](https://gitee.com/open_harmony/dashboard?issue_id=I40OPH) | 【定时服务】时间时区同步 | 特性基本可用 | | |
| [I40OPI](https://gitee.com/open_harmony/dashboard?issue_id=I40OPI) | 【定时服务】定时器功能 | 特性基本可用 | | |
| [I40OPJ](https://gitee.com/open_harmony/dashboard?issue_id=I40OPJ) | 【定时服务】时间时区管理 | 特性基本可用 | | |
| [I436VX](https://gitee.com/open_harmony/dashboard?issue_id=I436VX) | 提供分布式的回调Native接口 | 功能不可用 | 本地ability迁出到远端设备（标准设备之间）<br />遗留问题正在解决：<br />1，[I4C18L](https://gitee.com/openharmony/aafwk_standard/issues/I4C18L?from=project-issue)【OpenHarmony】【3.0.0.9】【用户程序框架子系统】迁出场景无法验收，拉起失败<br /><br />2，[I4CDNS](https://gitee.com/openharmony/aafwk_standard/issues/I4CDNS?from=project-issue)OpenHarmony】【LTS】【用户程序框架子系统】connectability 与dms联调，远程对象可以获取，端到端流程还没有打通 | |
| [I436N0](https://gitee.com/open_harmony/dashboard?issue_id=I436N0) | 支持应用包信息分布式存储能力 | 特性基本可用 |  | |
| [I436VH](https://gitee.com/open_harmony/dashboard?issue_id=I436VH) | 创建串行任务分发器，使用串行任务分发器执行任务 | 特性基本可用 | | |
| [I436VI](https://gitee.com/open_harmony/dashboard?issue_id=I436VI) | 创建专有任务分发器，使用专有任务分发器执行任务 | 特性基本可用 | | |
| [I436VJ](https://gitee.com/open_harmony/dashboard?issue_id=I436VJ) | 创建全局并发任务分发器，使用全局并发任务分发器执行任务 | 特性基本可用 | | |
| [I436VK](https://gitee.com/open_harmony/dashboard?issue_id=I436VK) | 创建并发任务分发器，使用并发任务分发器执行任务 | 特性基本可用 | | |
| [I436VT](https://gitee.com/open_harmony/dashboard?issue_id=I436VT) | 安装读取shortcut信息 | 特性基本可用 | 提供了解析config.json中shortcut，存储与读取能力 | |
| I4446U |【安全子系统】【权限管理】标准系统构建分布式权限同步能力 | 基本功能大部分无法验证，风险高 |依赖BMS实现获取应用前后台状态的接口 |BMS未实现获取应用前后台状态的接口导致功能无法完全验证|
| I441K6 |【安全子系统】【权限管理】标准系统构建分布式权限管理主体标识转换能力 | 基本功能正常，稳定性未测试，性能规格缺失 |依赖软总线 |前期软总线不稳定，导致稳定性无法测试，当前软总线已解决，重新验证中；|
| I4447R |【安全子系统】【权限管理】标准系统构建分布式权限检查能力 | 基本功能正常，稳定性未测试，性能规格缺失 |依赖软总线 |前期软总线不稳定，导致稳定性无法测试，当前软总线已解决，重新验证中；|
| I3YQBC |【安全子系统】【权限管理】PMS权限管理支持类型拓展  | 基本功能正常 |||
| [I474ZZ](https://gitee.com/openharmony/distributeddatamgr_appdatamgr/issues/I474ZZ) | 【分布式数据管理】（需求）实现具备CRUD的RDB JS接口 |  特性基本可用  | | 基本功能验证通过，自动化测试批量加载执行有问题，当前正在定位，不影响特性正常使用，xts用例暂时无法正常发布 |
|  |【软总线】基于BLE的发现 | 特性基本可用，遗留问题影响较小：[被动发布频繁更新蓝牙广播导致蓝牙资源耗尽，影响需要低功耗的被动发布](https://gitee.com/openharmony/communication_dsoftbus/issues/I4C3BE?from=project-issue) |依赖支持BLE的硬件设备 | 不满足转测条件（无版本，开发9-26提供了一个使用说明，需要自行编译测试版本，且需要dayu特定硬件设备，暂未开展），评估策略：开发提供自测用例和报告|
|  |【软总线】文件传输 | 特性基本可用 | | 功能用例执行Pass、可靠性/稳定性/性能用例执行中|
|  |【软总线】RPC及JS API| 特性基本可用，资料质量较差无法支撑开发者使用 | | 遗留资料问题：[I4BTW8](https://gitee.com/openharmony/communication_ipc/issues/I4BTW8) <br />不满足转测条件（依赖JS service ability：直接编译出来的Hap无法使用，需要手动修改并重新签名，暂未调通），评估策略：开发自测和依赖RPC的业务联调结果|
|I43I30|【图形子系统】vsync架构演进| 可以支持不同频率vsync的申请；特性基质量良好 | | |
|I43KL7|【图形子系统】合成器支持GPU渲染合成| 可以支持GPU合成，界面渲染正常；可通过日志打印EGL_version为private，确认GPU合成相关信息。命令为：进入hdc shell在hilog 中搜索 “GL version”；此特性不涉及对外接口，涉及GPU渲染，需要在3568开发板上才能生效；特性质量良好 |在3568上生效 | |
|I43V4W|【ACE子系统】支持使用JS开发service ability| 功能正常可用 | | |
|I43UU4|【ACE子系统】JS应用生命周期补充支持| 功能不可用 | [I4C18L](https://gitee.com/openharmony/aafwk_standard/issues/I4C18L?from=project-issue)【OpenHarmony】【3.0.0.9】【用户程序框架子系统】迁出场景无法验收，拉起失败<br /> | |
|I43V73|【ACE子系统】支持使用JS开发data ability| 测试功能正常可用，质量良好 | | |
|I43UZ0|【ACE子系统】支持系统服务弹窗| 功能正常可用，质量良好 | | |




## 4.2   兼容性测试结论

*补充兼容性测试报告*

## 4.3   安全专项测试结论

评估结论： Redirect<br/>
关键进展： <br/>
1、本轮测试覆盖OpenHarmony2.2到3.0所有涉及新增需求子系统，所有子系统已完成测试。当前还有11个安全issues没有关闭。<br/>
2、编码问题较为突出，静态告警和危险函数均未清零，静态告警主要集中在：用户程序框架，安全，媒体，电话服务等；危险函数主要集中在安全，分布式软总线，媒体子系统等。<br/>
3、有4个组件共23个公开漏洞未修复，主要集成在分布式软总线，安全子系统<br/>

当前遗留11个issues没有关闭，详细列表如下：
| 标题	   | 问题归属	| 归属子系统 | 状态     | 	URL                       | 
| -------- | ---------- | ---------- | -------- | --------------------------- | 
| 编码告警清理20210928	| 代码安全	| 驱动子系统	| 开启的	| https://gitee.com/openharmony/drivers_adapter/issues/I4C7M6?from=project-issue| 
| 【版本号：1.1.3】【3516】L1-3516.tar.gz_/L1-3516.tar_/obj/kernel/liteos_a/rootfs_vfat.img_/lib/libc++.so  SP 扫描结果是NO	| 编译选项	| 内核子系统	| 开启的| 	https://gitee.com/openharmony/kernel_liteos_a/issues/I4C34P?from=project-issue| 
| 【版本号：1.1.3】组件hostapd 2.9存在2个未修复漏洞	| 公开漏洞	| 软总线	| 待办的	| https://gitee.com/openharmony/third_party_wpa_supplicant/issues/I4AJ4C?from=project-issue| 
| 【版本号：1.1.3】组件wpa supplicant 2.9存在3个未修复漏洞	| 公开漏洞	| 软总线	| 待办的	| https://gitee.com/openharmony/third_party_wpa_supplicant/issues/I4AJB9?from=project-issue| 
| Softbus_server在执行socketfuzz时，出现crash	| 内存管理	| 分布式软总线子系统	| 技术评审中	| https://gitee.com/openharmony/communication_dsoftbus/issues/I480Z1?from=project-issue| 
| 【版本号：1.1.3】【3516;3518】libavcodec.so组件扫描出是DXX	| 开源组件	| DeviceManager组件| 	待办的| 	https://gitee.com/openharmony/device_hisilicon_third_party_ffmpeg/issues/I4AJI2?from=project-issue| 
| 【版本号：1.1.3】组件openssl 1.1.1存在14个未修复漏洞	| 公开漏洞	| 安全子系统|   待办的	| https://gitee.com/openharmony/third_party_openssl/issues/I4AJ9M?from=project-issue| 
| destroy_pake_server时没有从堆中将pin等敏感数据清除	| 信息泄露	| 安全子系统	| 待办的| 	https://gitee.com/openharmony/security_deviceauth/issues/I4BGP1?from=project-issue| 
| import_signed_auth_info_hilink导入的key用于HiChain连接时的身份标志，导入后加密存储在文件中，属于敏感数据，在返回后没有从堆中清除	| 信息泄露	| 安全子系统| 	待办的	| https://gitee.com/openharmony/security_deviceauth/issues/I4BGLS?from=project-issue| 
| 【版本号：1.1.3】【3516；3518】libopenssl_shared.so扫描出DXX	| 开源组件	| 安全子系统	| 待办的	| https://gitee.com/openharmony/third_party_openssl/issues/I4AJJA?from=project-issue| 
| 【版本号：1.1.3】组件mbed tls 2.16.8存在4个未修复漏洞	| 公开漏洞	| 安全子系统	| 待办的	| https://gitee.com/openharmony/third_party_mbedtls/issues/I4AJ6T?from=project-issue| 

## 4.4   稳定性专项测试结论

*当前稳定性专项只开展了开关机和xts压测专项，无设备稳定性问题发生，当前xts压测由于执行一轮全量测试需要的时间过长导致压测轮次数未达标*

| 测试分类 | 测试评估项 | 质量目标 | 是否满足 | 测试结果及关键遗留问题/风险 | 备注issue |
| -------- | ---------- | -------- | -------- | --------------------------- | --------- |
|     开关机测试     | 整机稳定性           | 无不开机、无整机稳定性问题         | 是          |测试结果：多个设备重启之后，hdc无法自动发现重启后的设备，需要重新kill掉hdc才能发现设备                             |   https://gitee.com/openharmony/developtools_hdc_standard/issues/I4BXYT?from=project-issue        |
|标准系统稳定性测试|xts用例压测|单轮压测6小时无稳定性问题，总共3轮压测|是|测试结果：无异常<br>风险：循环压测轮次未达标，单轮压测执行时间过长，当前无法满足循环次数要求</br>||


## 4.5   性能专项测试结论

*以表格形式汇总各专项测试执行情况及遗留问题情况的评估，给出专项质量评估结论*

*要求：1、静态KPI通过率100%，2、开关机及动态内存整机达标，子系统无严重问题

| 测试分类 | 测试评估项 | 质量目标 | 是否满足 | 测试结果及关键遗留问题/风险 | 备注issue |
| -------- | ---------- | -------- | -------- | --------------------------- | --------- |
| 基础性能 | 轻量系统静态模型 | 静态KPI通过率100% | 满足   |  轻量系统无感配网满足版本质量 |           |
| 基础性能 | 小型系统静态模型 | 静态KPI通过率100% | 不满足 |  小型系统图库、查看图片、拍照、录像均满足测试要求|   |
| 基础性能 | 标准系统静态模型 | 静态KPI通过率100% | 不满足 |  标准系统在当前版本设置、图库、商城、闹钟、计算机、相机、音乐的启动均劣化严重 | https://gitee.com/openharmony/community/issues/I4A10Q |
| 基础性能 | 轻量系统内存专项 | 开机以及动态内存整机达标，子系统无严重问题 | 满足   |  轻量系统无感配网满足版本质量 |           |
| 基础性能 | 小型系统内存专项 | 开机以及动态内存整机达标，子系统无严重问题 | 不满足 |  3516以及3518常驻内存均未达标 | https://gitee.com/openharmony/multimedia_camera_lite/issues/I4C7ZK https://gitee.com/openharmony/multimedia_camera_lite/issues/I434P1|
| 基础性能 | 标准系统静态模型 | 开机以及动态内存整机达标，子系统无严重问题 | 无法评估 |   当前系统内核380.51MB，常驻进程129.15MB | 标准系统无内存基线 |

## 4.6   功耗专项测试结论

*以表格形式汇总各专项测试执行情况及遗留问题情况的评估，给出专项质量评估结论*

*要求：无严重问题

| 测试分类 | 测试评估项 | 质量目标 | 是否满足 | 测试结果及关键遗留问题/风险 | 备注issue |
| -------- | ---------- | -------- | -------- | --------------------------- | --------- |
| 待机/场景功耗 | 小型系统待机/场景功耗 | 待机功耗达标 |  满足  | 待机/场景质量满足版本质量要 |           |
| 待机/场景功耗 | 标准系统待机/场景功耗 | 待机功耗达标 |  无法评估  | 标准系统版本间未劣化  | 标准系统无待机/场景功耗        |

# 5   问题单统计

[【OpenHarmony】【3.0.0.8】【轻内核子系统】集成测试I3NT3F内核支持trace功能缺少指导书无法测试，开发者使用难度较大 ](https://gitee.com/openharmony/kernel_liteos_a/issues/I4BN00)

[【OpenHarmony】【3.0.0.8】【轻内核子系统】dyload_posix模块在removefile的时候出现错误 ](https://gitee.com/openharmony/kernel_liteos_a/issues/I4BJFU)

[【WIP】【OpenHarmony】【3.0.0.2】【轻内核子系统】集成测试toybox需提供用户手册，手册中需要将各命令的限制详细说明，以便更好的指导... ](https://gitee.com/openharmony/third_party_toybox/issues/I44YLY)

[【OpenHarmony】【20210701】【轻内核子系统】xts权限用例压测用户态概率异常 ](https://gitee.com/openharmony/kernel_liteos_a/issues/I3ZJ1D)

[【OpenHarmony】【1.1.541】【轻内核子系统】集成测试toybox 命令kill、cp、mv等命令有些操作提示信息不合理，有些操作回显中有e..](https://gitee.com/openharmony/third_party_toybox/issues/I42VH1)

[【OpenHarmony】【20210726】【轻内核子系统】集成测试直接执行cat后无法退出，需要重启设备恢复](https://gitee.com/openharmony/third_party_mksh/issues/I42N33)

[【OpenHarmony】【3.0.0.9】【媒体子系统】视频录制后前几秒没声音，播放声画不同步，在板子播放会卡顿，音源较远时，杂音很大](https://gitee.com/openharmony/multimedia_camera_standard/issues/I4BXY1)

[【OpenHarmony】【3.0.0.9】【媒体子系统】音频录制后播放有杂音](https://gitee.com/openharmony/multimedia_media_standard/issues/I4BXWY)

[【OpenHarmony】【3.0.0.9】【媒体子系统】相机录制视频在图库播放时画面显示不全](https://gitee.com/openharmony/multimedia_camera_standard/issues/I4BXDQ)

[【OpenHarmony】【3.0.0.9】【媒体子系统】3516DV300/3518EV300 使用官网烧写配置媒体子系统(轻量小型)基本功能异常](https://gitee.com/openharmony/docs/issues/I4C8BO?from=project-issue)

[【OpenHarmony 3.0.0.9 】【驱动子系统】3516DV300单板camera驱动压测问题](https://gitee.com/openharmony/drivers_framework/issues/I4BWKC)

[【OpenHarmony 3.0.0.9 】【驱动子系统】3516DV300单板ResetDriver接口失败](https://gitee.com/openharmony/drivers_framework/issues/I4BW0G)

[【OpenHarmony 3.0 Beta1】【驱动子系统】不安全函数和安全编译选项](https://gitee.com/openharmony/drivers_peripheral/issues/I47DE6)

[【OpenHarmony 3.0.0.9】【DFX子系统】3516DV300单板调用hicollie接口失败](https://gitee.com/openharmony/hiviewdfx_hicollie/issues/I4BX4J)

[【OpenHarmony 3.0.0.9】【DFX子系统】3516DV300单板调用hitrace接口失败](https://gitee.com/openharmony/hiviewdfx_hitrace/issues/I4BX1X)

[运行hilog的压测，hilogd异常重启，且hilog命令一直无法使用](https://gitee.com/openharmony/hiviewdfx_hilog/issues/I48IM7)

[hdc_std在设备重启之后，需要hdc_std kill才能重新发现设备](https://gitee.com/openharmony/developtools_hdc_standard/issues/I4BXYT)

[【OpenHarmony】 【3.0.0.9】 【分布式数据子系统】 3516DV300单板 执行测试用例](https://gitee.com/openharmony/xts_acts/issues/I4C227)

[【3.0.0.9】【软总线-组网|短距】L2与手机_切换AP场景_发现组网成功率低(91%)，不达98%](https://gitee.com/openharmony/communication_dsoftbus/issues/I4BVUL)

[【3.0.0.9】【软总线-组网|HiChain】L2与手机_L2开关网络自组网成功率97%_失败3次_需分析失败原因](https://gitee.com/openharmony/communication_dsoftbus/issues/I4BVVW)

[【3.0.0.9】【软总线-组网|HiChain】L2与手机_手机侧循环开关网络_30次左右开始组网失败不再成功](https://gitee.com/openharmony/communication_dsoftbus/issues/I4BW6E)

[【3.0 beta1】【软总线-组网|HiChain】测试发现和组网性能(循环离网-发现-组网)_110次组网失败3次](https://gitee.com/openharmony/communication_dsoftbus/issues/I48YPH)

[【2.2 Beta2】【软总线-组网】组网10分钟自动下线规格，实际测试不准确，耗时多了30s左右](https://gitee.com/openharmony/communication_dsoftbus/issues/I4492M)

[【3.0 beta1】【软总线-发现】测试发现和组网性能(循环离网-发现-组网)_发现耗时999ms异常，开关机配网自组网触发的发现耗时超过500ms的占24%](https://gitee.com/openharmony/communication_dsoftbus/issues/I49HQ3)

[【3.0 beta1】【软总线-组网】开机首次自组网时间比开关网络自组网时间劣化1s（开机场景均值1.4s，开关网络场景均值0.4s）](https://gitee.com/openharmony/communication_dsoftbus/issues/I49HN6)

[【3.0 beta1】【软总线-传输】传输压测：3个线程间隔500ms发送数据_一段时间后大量返回-999（超时）_用例跑完很久_手机侧还在收到数据](https://gitee.com/openharmony/communication_dsoftbus/issues/I477AF)

[【OpenHarmony 3.0.0.9】【hiviewdfx子系统】3516DV300单板调用hicollie接口失败](https://gitee.com/openharmony/hiviewdfx_hicollie/issues/I4BX4J?from=project-issue)

[【OpenHarmony 3.0.0.9】【hiviewdfx子系统】3516DV300单板调用hitrace接口失败](https://gitee.com/openharmony/hiviewdfx_hitrace/issues/I4BX1X?from=project-issue)

[hdc_std在设备重启之后，需要hdc_std kill才能重新发现设备](https://gitee.com/openharmony/developtools_hdc_standard/issues/I4BXYT?from=project-issue)

[【OpenHarmony】【3.0.0.8】【性能测试】HI3516DV300 L1常驻内存超基线](https://gitee.com/openharmony/multimedia_camera_lite/issues/I4C7ZK)

[HI3516DV300 L1静态KPI部分指标劣化](https://gitee.com/openharmony/multimedia_camera_lite/issues/I434P1)

[【openHarmony】【3.0.0.6】性能测试L2-3516 3.0.0.6静态KPI劣化严重](https://gitee.com/openharmony/community/issues/I4A10Q)